<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Annotations\Annotation ;

/**
 * Agenda
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AgendaRepository")
 */

class Agenda
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="titre", type="string" , length=255 , nullable=true)
     */
    private $titre;

    /**
     * @var string
     * @ORM\Column(name="lieu", type="string" , length=255 , nullable=true)
     */
    private $lieu;

    /**
     * @var string
     * @ORM\Column(name="type", type="string" , length=255 , nullable=true)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="locale", type="string" , length=255 , nullable=true)
     */
    private $locale;

    /**
     * @var heure
     * @ORM\Column(name="tempsSortie", type="time" , length=255 , nullable=true)
     */
    private $tempsSortie;

    /**
     * @var text
     * @ORM\Column(name="description", type="text" , length=20000 , nullable=true)
     */
    private $description;

    /**
     * @var text
     * @ORM\Column(name="coupon", type="text" , length=20000 , nullable=true)
     */
    private $coupon;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Deplacement", inversedBy="Agenda"))
     * @ORM\JoinColumn(name="Deplacementid", referencedColumnName="id" , nullable=true)
     */
    private $Deplacementid;

    /**
     * @var ArrayCollection Produit $produits
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Produit", inversedBy="Agenda", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="produit_agenda",
     *   joinColumns={@ORM\JoinColumn(name="agenda", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="produit", referencedColumnName="id")}
     * )
     */
    private $Produitid;
   

    public function __construct()
    {
        $this->Produitid = new ArrayCollection();
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="Agenda"))
     * @ORM\JoinColumn(name="Clientid", referencedColumnName="id" , nullable=true)
     */
    private $Clientid;


    /**
     * @var heure
     * @ORM\Column(name="heure", type="time" , length=255 , nullable=true)
     */
    private $heure;

    /**
     * @var heure
     * @ORM\Column(name="heureR", type="time" , length=255 , nullable=true)
     */
    private $heureR;

    /**
     * @var heureD
     * @ORM\Column(name="heureD", type="time" , length=255 , nullable=true)
     */
    private $heureD;

    /**
     * @var date
     * @ORM\Column(name="date", type="date" , length=255 , nullable=true)
     */
    private $date;

    /**
     * @var date
     * @ORM\Column(name="dateex", type="date" , length=255 , nullable=true)
     */
    private $dateex;
    


    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payed", type="boolean", nullable=true)
     */
    private $payed;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return boolean
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @param boolean $payed
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;
    }

    /**
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * @return string
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param string $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }



    public function getDeplacementid()
    {
        return $this->Deplacementid;
    }

    /**
     * @param  $Deplacementid
     */
    public function setDeplacementid($Deplacementid)
    {
        $this->Deplacementid = $Deplacementid;
    }

    /**
     * @return $Produitid
     */
    public function getProduitid()
    {
        return $this->Produitid;
    }

    /**
     * @param  $Produitid
     */
    public function setProduitid($Produitid)
    {
        $this->Produitid = $Produitid;
    }
    /**
     * @return $Clientid
     */
    public function getClientid()
    {
        return $this->Clientid;
    }

    /**
     * @param  $Clientid
     */
    public function setClientid($Clientid)
    {
        $this->Clientid = $Clientid;
    }




    /**
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


    /**
     * @return date
     */
    public function getDateex()
    {
        return $this->dateex;
    }

    /**
     * @param date $dateex
     */
    public function setDateex($dateex)
    {
        $this->dateex = $dateex;
    }

    /**
     * @return heure
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * @param heure $heure
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;
    }

    /**
     * @return heure
     */
    public function getHeureD()
    {
        return $this->heureD;
    }

    /**
     * @param heure $heureD
     */
    public function setHeureD($heureD)
    {
        $this->heureD = $heureD;
    }

    /**
     * @return heure
     */
    public function getHeureR()
    {
        return $this->heureR;
    }

    /**
     * @param heure $heureR
     */
    public function setHeureR($heureR)
    {
        $this->heureR = $heureR;
    }

    /**
     * @return heure
     */
    public function getTempsSortie()
    {
        return $this->tempsSortie;
    }

    /**
     * @param heure $tempsSortie
     */
    public function setTempsSortie($tempsSortie)
    {
        $this->tempsSortie = $tempsSortie;
    }


    public function addProduit ($ptoduit)
    {
        $this->getProduitid()->add($ptoduit);
    }

    public function removeProduit ($ptoduit)
    {
        $this->getProduitid()->removeElement($ptoduit);
    }

}
