<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Produit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProduitRepository")
 */

class Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="designation", type="string" , length=255 , nullable=true)
     */
    private $designation;

    /**
     * @var float
     * @ORM\Column(name="prix", type="float"  , nullable=true)
     */
    private $prix;

    /**
     * @var string
     * @ORM\Column(name="dure", type="string" , length=255 , nullable=true)
     */
    private $dure;

    /**
     * @var integer
     * @ORM\Column(name="dureMin", type="integer" , nullable=true)
     */
    private $dureMin;
    
    /**
 * @var string
 *
 * @ORM\Column(name="logo", type="string", nullable=true)
 */
    private $logo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SousFamille", inversedBy="Produit"))
     * @ORM\JoinColumn(name="SousFamilleid", referencedColumnName="id" , nullable=true)
     */
    private $SousFamilleid;
    
    private $file;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }
    

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }
    

    /**
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param string $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return string
     */
    public function getDure()
    {
        return $this->dure;
    }

    /**
     * @param string $dure
     */
    public function setDure($dure)
    {
        $this->dure = $dure;
    }

    /**
     * @return integer
     */
    public function getDureMin()
    {
        return $this->dureMin;
    }

    /**
     * @param integer $dureMin
     */
    public function setDureMin($dureMin)
    {
        $this->dureMin = $dureMin;
    }

    /**
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }



    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getSousFamilleid()
    {
        return $this->SousFamilleid;
    }

    /**
     * @param mixed $SousFamilleid
     */
    public function setSousFamilleid($SousFamilleid)
    {
        $this->SousFamilleid = $SousFamilleid;
    }


   

  



}
