<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Famille
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FicheRepository")
 */

class Cheque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="coupon", type="string" , length=255 , nullable=true)
     */
    private $coupon;



    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="Cheque"))
     * @ORM\JoinColumn(name="Clientid", referencedColumnName="id" , nullable=true)
     */
    private $Clientid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cadeau", inversedBy="Cheque"))
     * @ORM\JoinColumn(name="cheque", referencedColumnName="id" , nullable=true)
     */
    private $cheque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payed", type="boolean", nullable=true)
     */
    private $payed;





    /**
 * @var string
 *
 * @ORM\Column(name="logo", type="string", nullable=true)
 */
    private $logo;


    
    private $file;
    /**
     * @var date
     * @ORM\Column(name="date", type="date" , length=255 , nullable=true)
     */
    private $date;


    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return boolean
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @param boolean $payed
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;
    }


    /**
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return $Clientid
     */
    public function getClientid()
    {
        return $this->Clientid;
    }

    /**
     * @param  $Clientid
     */
    public function setClientid($Clientid)
    {
        $this->Clientid = $Clientid;
    }
    /**
     * @return $cheque
     */
    public function getCheque()
    {
        return $this->cheque;
    }

    /**
     * @param  $cheque
     */
    public function setCheque($cheque)
    {
        $this->cheque = $cheque;
    }


    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }



    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }
    


    

    /**
     * @return string
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param string $designation
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }




   

  



}
