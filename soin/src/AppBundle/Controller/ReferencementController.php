<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Metas;
use AppBundle\Form\AutreMetaForm;
use AppBundle\Form\MetaForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ReferencementController extends Controller
{

    public function listMetasAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Metas m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/metas.html.twig",array('pagination' => $results));
    }

    public function ajoutMetasAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $meta = new Metas();
        $form = $this->createForm(MetaForm::class, $meta);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $meta->setIsDeleted(1);
            $em->persist($meta);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Méta ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $meta = new Metas();
                $form = $this->createForm(MetaForm::class, $meta);
                return $this->render('default/ajoutMeta.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('kalitys_crm_list_referencement'));
        }
        return $this->render('default/ajoutMeta.html.twig', array('form' => $form->createView(),'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function deleteMetaAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $metas = $em->getRepository('AppBundle:Metas')->find($id);
        $metas->setIsDeleted(0);
        $em->persist($metas);
        $em->flush();
        return $this->redirect($this->generateUrl("kalitys_crm_list_referencement"));
    }

    public function updateMetaAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $metas = $em->getRepository('AppBundle:Metas')->find($id);
        $form = $this->createForm(MetaForm::class, $metas);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $em->persist($metas);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Métas modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("kalitys_crm_list_referencement"));

        }
        return $this->render('default/ajoutMeta.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

    public function updateAutreMetaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $metas = $em->getRepository('AppBundle:AutreMetas')->find(1);
        $form = $this->createForm(AutreMetaForm::class, $metas);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $em->persist($metas);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Métas modofiée avec succés");
            $this->setFlash($flash);

        }
        return $this->render('default/autreMeta.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

}
