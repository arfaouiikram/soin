<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class FamilleController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Famille m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/famille.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $famille = new Famille();
        $form = $this->createForm(FamilleForm::class, $famille);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $famille->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/famille',
                    $fileName
                );
                $famille->setLogo($fileName);
                $famille->setFile(null);
            }
            $famille->setIsDeleted(1);
            $em->persist($famille);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $famille = new Famille();
                $form = $this->createForm(FamilleForm::class, $famille);
                return $this->render('default/ajoutFamille.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_famille'));
        }
        return $this->render('default/ajoutFamille.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository('AppBundle:Famille')->find($id);
        $famille->setIsDeleted(0);
        $em->persist($famille);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_famille"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository('AppBundle:Famille')->find($id);
        $form = $this->createForm(FamilleForm::class, $famille);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $famille->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/famille',
                    $fileName
                );
                $famille->setLogo($fileName);
                $famille->setFile(null);
            }
            $em->persist($famille);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_famille"));

        }
        return $this->render('default/ajoutFamille.html.twig', array('form' => $form->createView(),'logo'=>$famille->getLogo(),'display'=>'none'));
    }

}
