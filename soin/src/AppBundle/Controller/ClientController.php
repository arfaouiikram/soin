<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Adresse;
use AppBundle\Entity\Client;
use AppBundle\Form\AdresseForm;
use AppBundle\Form\ClientForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ClientController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:User m where m.enabled=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/client.html.twig",array('pagination' => $results));
    }

    public function adresseAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Adresse m join m.ClientId c where m.isDeleted=1 and c.id=".$id ;
        $query = $em->createQuery($dql);
        $client = $em->getRepository('AppBundle:User')->find($id);

        $results = $query->getResult();
        return $this->render("default/adresse.html.twig",array('pagination' => $results,'client'=>$client));
    }

    public function ajoutAdresseAction(Request $request ,$id) {
        $em = $this->getDoctrine()->getManager();
        $adresse = new Adresse();
        $client = $em->getRepository('AppBundle:User')->find($id);
        $form = $this->createForm(AdresseForm::class, $adresse);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $adresse->setIsDeleted(1);
            $adresse->setClientId($client);
            $em->persist($adresse);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Adresse ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $adresse = new Adresse();
                $form = $this->createForm(AdresseForm::class, $adresse);
                return $this->render('default/ajoutAdresse.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block','client'=>$client));
            }
            return $this->redirect($this->generateUrl('simulation_crm_adresse_client',array('id'=>$id)));
        }
        return $this->render('default/ajoutAdresse.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block','client'=>$client));
    }

    public function ficheAction(Request $request ,$id) {
        $em = $this->getDoctrine()->getManager();
        $adresse = new Adresse();
        $client = $em->getRepository('AppBundle:User')->find($id);
        $fiche = $em->getRepository('AppBundle:Fiche')->findBy(array('Clientid'=>$client));
        return $this->render('default/fiche.html.twig', array('fiche'=>$fiche?$fiche[0]:null));
    }

    public function ajoutClientAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $client = new Client();
        $form = $this->createForm(ClientForm::class, $client);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $client->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/client',
                    $fileName
                );
                $client->setLogo($fileName);
                $client->setFile(null);
            }
            $client->setIsDeleted(1);
            $em->persist($client);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $client = new Client();
                $form = $this->createForm(ClientForm::class, $client);
                return $this->render('default/ajoutClient.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_client'));
        }
        return $this->render('default/ajoutClient.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function deleteClientAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:User')->find($id);
        $client->setEnabled(0);
        $em->persist($client);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_client"));
    }
    public function supprimerAdresseAction(Request $request , $id, $idClient){
        $em = $this->getDoctrine()->getManager();
        $adresse = $em->getRepository('AppBundle:Adresse')->find($id);
        $adresse->setIsDeleted(0);
        $em->persist($adresse);
        $em->flush();
        return $this->redirect($this->generateUrl("simulation_crm_adresse_client",array('id'=>$idClient)));
    }


    public function updateClientAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:User')->find($id);
        $form = $this->createForm(ClientForm::class, $client);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $client->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/client',
                    $fileName
                );
                $client->setLogo($fileName);
                $client->setFile(null);
            }
            $em->persist($client);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_client"));

        }
        return $this->render('default/ajoutClient.html.twig', array('form' => $form->createView(),'logo'=>$client->getLogo(),'display'=>'none'));
    }

    public function modifierAdresseAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $adresse = $em->getRepository('AppBundle:Adresse')->find($id);
        $form = $this->createForm(AdresseForm::class, $adresse);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            $em->persist($adresse);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Adresse modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("simulation_crm_adresse_client",array('id'=>$adresse->getClientId()->getId())));

        }
        return $this->render('default/ajoutAdresse.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

}
