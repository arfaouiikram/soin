<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Actualite;
use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class ContactController extends Controller
{

    public function indexAction(Request $request)
    {
        return $this->render("default/frontContact.html.twig");
    }

    public function contactAction(Request $request) {
        $to = "arfaouiikram2015@gmail.com";
        $subject = "contact rapide";
        $txt = 'Vous avez un nouveau conatct '."\r\n".'mail : '.$request->request->get('form_email')."\r\n".'message : '.$request->request->get('form_message');
        $headers = "From: ".$request->request->get('form_email')."\r\n".
        "Cc : hajer.hammami.ing@gmail.com";
        mail($to,$subject,$txt,$headers);
        $flash = array(
            'key' => 'warning',
            'title' => 'Succés',
            'msg' => "votre message a été bien transmis");
        $this->setFlash($flash);
        return $this->redirectToRoute('kalitys_crm_homepage');
    }

    public  function verifMailAction(Request $request){
        $to = $request->request->get('mail');
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->findBy(array('email'=>$to));

        if(sizeof($user)!=0){
            $subject = "Vérification mail";
            $txt = "Pour changer votre mot de passe clickez ici :"."\r\n"."http://test-dewinter.com/belle-zen/soin/soin/web/contact/verification/".$user[0]->getId();
            $headers = "From: Belle&Zen";
            mail($to,$subject,$txt,$headers);
            $flash = array(
                'key' => 'warning',
                'title' => 'Succés',
                'msg' => "Vous pouvez vérifier votre boite mail pour Login");
            $this->setFlash($flash);
        }else{
            $flash = array(
                'key' => 'warning',
                'title' => 'Échec',
                'msg' => "cet utulisateur n'existe pas , vérifier votre mail .");
            $this->setFlash($flash);
        }
        return $this->redirectToRoute('fos_user_security_login');
    }
    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('mail', $value);
    }

    public  function  verificationAction(Request $request ,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        return $this->render("default/verification.html.twig",array('id'=>$id));
    }

    public function passwordAction(Request $request,$id){
        $motdePasse=$request->request->get('password');
        $motdePasse1=$request->request->get('password1');
        if($motdePasse!=$motdePasse1){
            $flash = array(
                'key' => 'warning',
                'title' => 'Échec',
                'msg' => "les mot de passe ne sont pas identiques.");
            $this->setFlash($flash);
            return $this->redirectToRoute('soin_crm_verification',array('id'=>$id));
        }
        if($motdePasse=="" || $motdePasse1==""){
            $flash = array(
                'key' => 'warning',
                'title' => 'Échec',
                'msg' => "champs mot de passe est vide.");
            $this->setFlash($flash);
            return $this->redirectToRoute('soin_crm_verification',array('id'=>$id));
        }
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setPassword($motdePasse);
        $user->setPlainPassword($motdePasse);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('fos_user_security_login');
    }




}
