<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Agenda;
use AppBundle\Entity\Client;
use AppBundle\Form\AgendaForm;
use AppBundle\Form\ClientForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;


class AgendaController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Agenda m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/agenda.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $agenda = new Agenda();
        $form = $this->createForm(AgendaForm::class, $agenda);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $existe=0;
            $rendezVous = $em->getRepository('AppBundle:Agenda')->findBy(array('isDeleted'=>1));
            if($agenda->getType()==1){
                $nbHeure=$agenda->getDeplacementid()->getHeure();
                $nbMinute=$agenda->getDeplacementid()->getMinute();
                $time=$agenda->getHeure();
                $timeRetour=$agenda->getHeureD();
                $heure=$agenda->getHeure()->format('H');
                $minute=$agenda->getHeure()->format('I');
                $heureD=$agenda->getHeureD()->format('H');
                $minuteD=$agenda->getHeureD()->format('I');
                if($nbHeure!=null){
                    $time->sub(new \DateInterval('PT'.$nbHeure.'H'));
                    $timeRetour->add(new \DateInterval('PT'.$nbHeure.'H'));

                }
                if($nbMinute!=null){
                    $time->sub(new \DateInterval('PT'.$nbMinute.'M'));
                    $timeRetour->add(new \DateInterval('PT'.$nbMinute.'M'));
                }
            }
            $date=new \DateTime();
            $dateD=new \DateTime();
            $date->setTime($heure,$minute);
            $dateD->setTime($heureD,$minuteD);
            $agenda->setIsDeleted(1);
            $agenda->setTempsSortie($time);
            $agenda->setHeureR($timeRetour);
            $agenda->setHeure($date);
            $agenda->setHeureD($agenda->getHeure());
            foreach($rendezVous as $rend){
                if($rend->getDate()==$agenda->getDate()){
                    if(($agenda->getTempsSortie()<=$rend->getHeureR())&&($agenda->getTempsSortie()>=$rend->getTempsSortie())){
                        $existe=1;
                    }
                }
            }
            if($existe==1){
                $flash = array(
                    'key' => 'warning',
                    'title' => 'Échec',
                    'msg' => "Vous n'êtes pas disponible");
                $this->setFlash($flash);

            }else{
                $em->persist($agenda);
                $em->flush();

                $nextAction = $request->request->has("ajout");
                if($nextAction==true)
                {
                    $client = new Agenda();
                    $form = $this->createForm(AgendaForm::class, $client);
                    return $this->render('default/ajoutAgenda.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
                }
                return $this->redirect($this->generateUrl('soin_crm_agenda'));
            }

        }
        return $this->render('default/ajoutAgenda.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $agenda = $em->getRepository('AppBundle:Agenda')->find($id);
        $agenda->setIsDeleted(0);
        $em->persist($agenda);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_agenda"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $agenda = $em->getRepository('AppBundle:Agenda')->find($id);
        $form = $this->createForm(AgendaForm::class, $agenda);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($agenda->getType()==1){
                $nbHeure=$agenda->getDeplacementid()->getHeure();
                $nbMinute=$agenda->getDeplacementid()->getMinute();
                $time=$agenda->getHeure();
                $timeRetour=$agenda->getHeureD();
                $heure=$agenda->getHeure()->format('H');
                $minute=$agenda->getHeure()->format('I');
                $heureD=$agenda->getHeureD()->format('H');
                $minuteD=$agenda->getHeureD()->format('I');
                if($nbHeure!=null){
                    $time->sub(new \DateInterval('PT'.$nbHeure.'H'));
                    $timeRetour->add(new \DateInterval('PT'.$nbHeure.'H'));

                }
                if($nbMinute!=null){
                    $time->sub(new \DateInterval('PT'.$nbMinute.'M'));
                    $timeRetour->add(new \DateInterval('PT'.$nbMinute.'M'));
                }
            }
            $date=new \DateTime();
            $dateD=new \DateTime();
            $date->setTime($heure,$minute);
            $dateD->setTime($heureD,$minuteD);
            $agenda->setTempsSortie($time);
            $agenda->setHeureR($timeRetour);
            $agenda->setHeure($date);
            $agenda->setHeureD($dateD);
            $em->persist($agenda);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Rendez-vous modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_agenda"));

        }
        return $this->render('default/ajoutAgenda.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

}
