<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Entity\Galerie;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use AppBundle\Form\GalerieForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class GalerieController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Galerie m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/galerie.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $galerie = new Galerie();
        $form = $this->createForm(GalerieForm::class, $galerie);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $galerie->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/galerie',
                    $fileName
                );
                $galerie->setLogo($fileName);
                $galerie->setFile(null);
            }
            $galerie->setIsDeleted(1);
            $em->persist($galerie);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $galerie = new Galerie();
                $form = $this->createForm(GalerieForm::class, $galerie);
                return $this->render('default/ajoutGalerie.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_galerie'));
        }
        return $this->render('default/ajoutGalerie.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $galerie = $em->getRepository('AppBundle:Galerie')->find($id);
        $galerie->setIsDeleted(0);
        $em->persist($galerie);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_galerie"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $galerie = $em->getRepository('AppBundle:Galerie')->find($id);
        $form = $this->createForm(GalerieForm::class, $galerie);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $galerie->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/galerie',
                    $fileName
                );
                $galerie->setLogo($fileName);
                $galerie->setFile(null);
            }
            $em->persist($galerie);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_galerie"));

        }
        return $this->render('default/ajoutGalerie.html.twig', array('form' => $form->createView(),'logo'=>$galerie->getLogo(),'display'=>'none'));
    }

    public function afficheAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $galerie = $em->getRepository('AppBundle:Galerie')->findBy(array('isDeleted'=>1));
            return $this->render('default/frontGalerie.html.twig',array('pagination'=>$galerie));
    }

}
