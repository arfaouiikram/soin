<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Actualite;
use AppBundle\Entity\Agenda;
use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;


class MassageController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $session=new Session();
        $dql = "SELECT m "
            . "FROM AppBundle:Produit m join m.SousFamilleid sf join sf.Familleid f where m.isDeleted=1 and f.id=2" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $pagination=array();
        foreach ($results as $res){
            $produit=$session->get($res->getId());
            if($produit){
                $pagination[]=['achat'=>'none','prod'=>$res];
            }else{
                $pagination[]=['achat'=>'block','prod'=>$res];
            }
        }
        return $this->render("default/frontMassage.html.twig",array('pagination'=>$pagination));
    }



    public function ajoutAction(Request $request) {
        $titre=$request->request->get('title');
        $debut=$request->request->get('start')+"";
        $year=$request->request->get('year')+"";
        $month=$request->request->get('month')+"";
        $day=$request->request->get('day')+"";
        $hour=$request->request->get('hour')+"";
        $second=$request->request->get('second')+"";
        $fin=$request->request->get('end');

        $agenda = new Agenda();
        $user=$this->getUser();
        $agenda->setDate(new \DateTime($year."-".$month."-".$day));
        $agenda->setHeure(new \DateTime($hour.":".$second));
        $agenda->setTitre($titre);
        $agenda->setDescription($titre);
        $agenda->setIsDeleted(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($agenda);
        $em->flush();
        return new Response($year." ".$month." ".$day." ".$hour." ".$second);
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $actualite->setIsDeleted(0);
        $em->persist($actualite);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_actuliate"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $form = $this->createForm(ActualiteForm::class, $actualite);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $actualite->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/actualite',
                    $fileName
                );
                $actualite->setLogo($fileName);
                $actualite->setFile(null);
            }
            $em->persist($actualite);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_actualite"));

        }
        return $this->render('default/ajoutActualite.html.twig', array('form' => $form->createView(),'logo'=>$actualite->getLogo(),'display'=>'none'));
    }

}
