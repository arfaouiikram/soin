<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Actualite;
use AppBundle\Entity\Agenda;
use AppBundle\Entity\Client;
use AppBundle\Entity\DeplacementRepository;
use AppBundle\Entity\Famille;
use AppBundle\Entity\Fiche;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use AppBundle\Form\FicheForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;


class SessionController extends Controller
{

    public function indexAction(Request $request,$id,$url)
    {
        $session = new Session();
        $session->set($id, $id);
        return new Response('yup');
    }

    public function achatAction(Request $request)
    {

         //if($this->getUser()){

             $em = $this->getDoctrine()->getManager();
            $fiche = $em->getRepository('AppBundle:Fiche')->findBy(array('Clientid'=>$this->getUser()));
            if(sizeof($fiche)!=0){
                $form1 = $this->createForm(FicheForm::class, $fiche[0]);
            }else{
                $fiche=new Fiche();
                $form1 = $this->createForm(FicheForm::class, $fiche);
            }
            $session = new Session();
            $form = $this->createFormBuilder()
                ->add('Deplacementid', EntityType::class, array(
                    'class' => 'AppBundle:Deplacement',
                    'query_builder' => function (DeplacementRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->where('u.isDeleted=1')
                            ->orderBy('u.designation', 'ASC');
                    },
                    'choice_label' => 'designation',
                    'placeholder' => 'Sélectionner une Ville',
                    'empty_data'  => null,
                ))
                ->add('locale', ChoiceType::class, array(
                    'choices'  => array(
                        'Institut' => '1',
                        'À Domicile' => '2',
                    ),
                ))
                ->add('heure', TimeType::class, array(
                    'widget' => 'single_text',
                ))
                ->add('date', \Symfony\Component\Form\Extension\Core\Type\DateType::class, array(
                    'widget' => 'single_text',
                ))
                ->add('titre')
                ->getForm();
            $somme=0;
            $results = $em->getRepository('AppBundle:Produit')->findAll();
            $pagination=array();
            foreach ($results as $res){
                $produit=$session->get($res->getId());
                if($produit){
                    $pagination[]=$res;
                    $somme+=$res->getPrix();
                }
            }
            $visible='none';
            if(sizeof($pagination)!=0){
                $visible='block';
            }
            return $this->render("default/frontAchat.html.twig",array('pagination'=>$pagination,'somme'=>$somme,'form'=>$form->createView(),'visible'=>$visible,'form1'=>$form1->createView()));

       // }
       // return $this->redirectToRoute("fos_user_security_login");


    }



    public function ajoutAction(Request $request) {
        $titre=$request->request->get('title');
        $debut=$request->request->get('start')+"";
        $year=$request->request->get('year')+"";
        $month=$request->request->get('month')+"";
        $day=$request->request->get('day')+"";
        $hour=$request->request->get('hour')+"";
        $second=$request->request->get('second')+"";
        $fin=$request->request->get('end');

        $agenda = new Agenda();
        $user=$this->getUser();
        $agenda->setDate(new \DateTime($year."-".$month."-".$day));
        $agenda->setHeure(new \DateTime($hour.":".$second));
        $agenda->setTitre($titre);
        $agenda->setDescription($titre);
        $agenda->setIsDeleted(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($agenda);
        $em->flush();
        return new Response($year." ".$month." ".$day." ".$hour." ".$second);
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $session=new Session();
        $session->remove($id);
        return $this->redirectToRoute('soin_crm_achat');
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $form = $this->createForm(ActualiteForm::class, $actualite);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $actualite->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/actualite',
                    $fileName
                );
                $actualite->setLogo($fileName);
                $actualite->setFile(null);
            }
            $em->persist($actualite);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_actualite"));

        }
        return $this->render('default/ajoutActualite.html.twig', array('form' => $form->createView(),'logo'=>$actualite->getLogo(),'display'=>'none'));
    }

    public function ficheAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if($this->getUser()){
            $fiche = $em->getRepository('AppBundle:Fiche')->findBy(array('Clientid'=>$this->getUser()));
            if(sizeof($fiche)==0){
                $fiche=new Fiche();
            }else{
                $fiche=$fiche[0];
            }
            $form1=$request->request->get('fiche_form');
            $designation=$form1['designation'];
            $description=$form1['description'];
            $abonnement=$form1['abonnement'];
            $fiche->setDesignation($designation);
            $fiche->setDescription($description);
            $fiche->setAbonnement($abonnement);
            $fiche->setClientid($this->getUser());
            $em->persist($fiche);
            $em->flush();
            return $this->redirectToRoute('soin_crm_achat');

        }
        return $this->redirectToRoute("fos_user_security_login");

    }

    public function acceeFicheAction(Request $request,$somme){

        return $this->render('default/frontAcceeFiche.html.twig',array('somme'=>$somme));

    }



}
