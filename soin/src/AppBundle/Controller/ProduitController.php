<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Entity\Logo;
use AppBundle\Entity\Produit;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use AppBundle\Form\LogoForm;
use AppBundle\Form\ProduitForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ProduitController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Produit m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/produit.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $produit = new Produit();
        $form = $this->createForm(ProduitForm::class, $produit);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $produit->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/produit',
                    $fileName
                );
                $produit->setLogo($fileName);
                $produit->setFile(null);
            }
            $produit->setIsDeleted(1);
            $em->persist($produit);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $produit = new Produit();
                $form = $this->createForm(ProduitForm::class, $produit);
                return $this->render('default/ajoutProduit.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_produit'));
        }
        return $this->render('default/ajoutProduit.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        $produit->setIsDeleted(0);
        $em->persist($produit);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_produit"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        $form = $this->createForm(ProduitForm::class, $produit);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $produit->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/produit',
                    $fileName
                );
                $produit->setLogo($fileName);
                $produit->setFile(null);
            }
            $em->persist($produit);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_produit"));

        }
        return $this->render('default/ajoutProduit.html.twig', array('form' => $form->createView(),'logo'=>$produit->getLogo(),'display'=>'none'));
    }

    public  function imagesAction(Request $request , $id){
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Logo  m join m.Produitid p where m.isDeleted=1 and p.id=".$id;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/logo.html.twig", array('pagination' => $results,'id'=>$id));
    }

    public function ajoutImageAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $logo = new Logo();
        $form = $this->createForm(LogoForm::class, $logo);
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $logo->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/logo',
                    $fileName
                );
                $logo->setSrc($fileName);
                $logo->setFile(null);
            }
            $logo->setIsDeleted(1);
            $logo->setProduitid($produit);
            $em->persist($logo);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Logo ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if ($nextAction == true) {
                $logo = new Logo();
                $form = $this->createForm(LogoForm::class, $logo);
                return $this->render('default/ajoutLogo.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block','id'=>$id));
            }
            return $this->redirect($this->generateUrl('soin_crm_images_produit',array('id'=>$id)));
        }
        return $this->render('default/ajoutLogo.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'block','id'=>$id));
    }
    public function modifierImageAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $logo = $em->getRepository('AppBundle:Logo')->find($id);
        $form = $this->createForm(LogoForm::class, $logo);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $logo->getFile();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/image/logo',
                    $fileName
                );
                $logo->setSrc($fileName);
                $logo->setFile(null);
            }
            $logo->setIsDeleted(1);
            $em->persist($logo);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Logo ajoutée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('soin_crm_images_produit',array('id'=>$logo->getProduitid()->getId())));
        }
        return $this->render('default/ajoutLogo.html.twig', array('form' => $form->createView(), 'logo' => null, 'display' => 'none','id'=>$id));
    }

    public function supprimerImageAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Logo')->find($id);
        $type->setIsDeleted(0);
        $em->persist($type);
        $em->flush();
        return $this->redirect($this->generateUrl('soin_crm_images_produit',array('id'=>$type->getProduitid()->getId())));
    }

}
