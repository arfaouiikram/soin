<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Entity\SousFamille;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use AppBundle\Form\SousFamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class SousFamilleController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:SousFamille m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/sousfamille.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $sousfamille = new SousFamille();
        $form = $this->createForm(SousFamilleForm::class, $sousfamille);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $sousfamille->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/sousfamille',
                    $fileName
                );
                $sousfamille->setLogo($fileName);
                $sousfamille->setFile(null);
            }
            $sousfamille->setIsDeleted(1);
            $em->persist($sousfamille);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $sousfamille = new SousFamille();
                $form = $this->createForm(SousFamilleForm::class, $sousfamille);
                return $this->render('default/ajoutSousFamille.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_sous_famille'));
        }
        return $this->render('default/ajoutSousFamille.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $sousfamille = $em->getRepository('AppBundle:SousFamille')->find($id);
        $sousfamille->setIsDeleted(0);
        $em->persist($sousfamille);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_sous_famille"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $sousfamille = $em->getRepository('AppBundle:SousFamille')->find($id);
        $form = $this->createForm(SousFamilleForm::class, $sousfamille);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $sousfamille->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/sousfamille',
                    $fileName
                );
                $sousfamille->setLogo($fileName);
                $sousfamille->setFile(null);
            }
            $em->persist($sousfamille);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_sous_famille"));

        }
        return $this->render('default/ajoutSousFamille.html.twig', array('form' => $form->createView(),'logo'=>$sousfamille->getLogo(),'display'=>'none'));
    }

}
