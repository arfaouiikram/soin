<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\Deplacement;
use AppBundle\Entity\Famille;
use AppBundle\Form\ClientForm;
use AppBundle\Form\DeplacementForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class DeplacementController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Deplacement m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/deplacement.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $deplacement = new Deplacement();
        $form = $this->createForm(DeplacementForm::class, $deplacement);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $deplacement->setIsDeleted(1);
            $deplacement->setTps($deplacement->__toString());
            $em->persist($deplacement);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $deplacement = new Deplacement();
                $form = $this->createForm(DeplacementForm::class, $deplacement);
                return $this->render('default/ajoutDeplacement.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_deplacement'));
        }
        return $this->render('default/ajoutDeplacement.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $deplacement = $em->getRepository('AppBundle:Deplacement')->find($id);
        $deplacement->setIsDeleted(0);
        $em->persist($deplacement);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_deplacement"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $deplacement = $em->getRepository('AppBundle:Deplacement')->find($id);
        $form = $this->createForm(DeplacementForm::class, $deplacement);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $deplacement->setTps($deplacement->__toString());
            $em->persist($deplacement);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_deplacement"));

        }
        return $this->render('default/ajoutDeplacement.html.twig', array('form' => $form->createView(),'display'=>'none'));
    }

}
