<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Form\ClientForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\RechForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class RechercheController extends Controller
{

    public function RechercheAction(Request $request,$marque,$model,$cylindree,$puissance,$typeMoteur)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT m "
            . "FROM AppBundle:Produit m where m.isDeleted=1 and m.topNov=1" ;
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->createQuery($dql);
        $results1=$query->getResult();
        if($marque!=null && $marque!=0)
            $dql.=" and m.Marqueid=".$marque;
        if($model!=null && $model!=0)
            $dql.=" and m.Modeleid=".$model;
        if($cylindree!=null && $cylindree!=0)
            $dql.=" and  m.Cylindreeid=".$cylindree;
        if($puissance!=null && $puissance!=0)
            $dql.=" and  m.Puissanceid=".$puissance;
        if($typeMoteur!=null && $typeMoteur!=0)
            $dql.=" and  m.Type_moteurid=".$typeMoteur;
        $dql.=" and m.isDeleted=1";
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $form = $this->createForm(RechForm::class);
        $user=$this->getUser();
        $display='block';
        $display1='none';
        if($user!=null)
        {
            $display='none';
            $display1='block';
        }
        return $this->render("default/recherche.html.twig",array('pagination' => $results,'pagination1'=>$results1,'display'=>$display,'display1'=>$display1,'form'=>$form->createView()));
    }

     public function RechercheProduitAction(Request $request){
         $em = $this->getDoctrine()->getManager();
         $nom=$request->query->get('search');
         $dql = "SELECT m "
             . "FROM AppBundle:Produit m where m.isDeleted=1 and m.designation='".$nom."'" ;
         $query = $em->createQuery($dql);
         $results = $query->getResult();
         $form = $this->createForm(RechForm::class);
         $user=$this->getUser();
         $display='block';
         $display1='none';
         if($user!=null)
         {
             $display='none';
             $display1='block';
         }
         return $this->render("default/recherche.html.twig",array('pagination' => $results,'display'=>$display,'display1'=>$display1,'form'=>$form->createView()));
     }

    public function getModeleAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Modele  m join m.Marqueid ma where m.isDeleted=1 and ma.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $data=array();
        $i=0;
        foreach ($results as $modele ) {
            $data[$i++]=["key"=>$modele->getId(),"value"=>$modele->getDesignation()];
        }
        return new JsonResponse($data);

    }

    public function getCylindreeAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Cylindree  m join m.Modeleid ma where m.isDeleted=1 and ma.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $data=array();
        $i=0;
        foreach ($results as $modele ) {
            $data[$i++]=["key"=>$modele->getId(),"value"=>$modele->getDesignation()];
        }
        return new JsonResponse($data);

    }

    public function getPuissanceAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Puissance  m join m.Cylindreeid ma where m.isDeleted=1 and ma.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $data=array();
        $i=0;
        foreach ($results as $modele ) {
            $data[$i++]=["key"=>$modele->getId(),"value"=>$modele->getDesignation()];
        }
        return new JsonResponse($data);

    }

    public function getTypeAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Type_moteur  m join m.Puissanceid ma where m.isDeleted=1 and ma.id=".$id ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        $data=array();
        $i=0;
        foreach ($results as $modele ) {
            $data[$i++]=["key"=>$modele->getId(),"value"=>$modele->getDesignation()];
        }
        return new JsonResponse($data);

    }




}
