<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Actualite;
use AppBundle\Entity\Agenda;
use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;



class CalendrierController extends Controller
{

    public function indexAction(Request $request )
    {

        if ($this->getUser()){
            $em = $this->get('doctrine.orm.entity_manager');
            if($this->getUser()->isSuperAdmin()){
                $dql = "SELECT m "
                    . "FROM AppBundle:Agenda m  where m.isDeleted=1 " ;
            }else{
                $dql = "SELECT m "
                    . "FROM AppBundle:Agenda m join m.Clientid u where m.isDeleted=1 and u.id=".$this->getUser()->getId() ;
            }

            $query = $em->createQuery($dql);
            $results = $query->getResult();
            /*$produits = $em->getRepository('AppBundle:Produit')->findAll();
            $pagination=array();
            $session = new Session();
            $somme=0;
            $dep=$session->get('dep');
            $deplacement = $em->getRepository('AppBundle:Deplacement')->find($dep);
            if($deplacement){
                $somme+=$deplacement->getTarif();
                if($deplacement->getType()==1){
                    $somme+=60;
                }elseif($deplacement->getType()==2){
                    $somme+=40;
                }else{
                    $somme+=20;
                }
            }

            foreach ($produits as $res){
                $produit=$session->get($res->getId());
                if($produit){
                    $pagination[]=$res;
                    $somme+=$res->getPrix();
                }
            }*/
            return $this->render("default/frontCalendrier.html.twig",array('pagination'=>$results));
        }
        return $this->redirectToRoute("fos_user_security_login");
    }

    public function listAction(Request $request )
    {
        if ($this->getUser()){
            $em = $this->get('doctrine.orm.entity_manager');
            if($this->getUser()->isSuperAdmin()){
                $dql = "SELECT m "
                    . "FROM AppBundle:Agenda m  where m.isDeleted=1 " ;
                $dql1 = "SELECT m "
                    . "FROM AppBundle:Cheque m  where m.isDeleted=1 " ;
            }else{
                $dql = "SELECT m "
                    . "FROM AppBundle:Agenda m join m.Clientid u where m.isDeleted=1 and u.id=".$this->getUser()->getId() ;
                $dql1 = "SELECT m "
                    . "FROM AppBundle:Cheque m join m.Clientid u where m.isDeleted=1 and u.id=".$this->getUser()->getId() ;
            }
            $query = $em->createQuery($dql);
            $results = $query->getResult();
            $query1 = $em->createQuery($dql1);
            $results1 = $query1->getResult();
            $display='none';
            $display1='none';
            if(sizeof($results)!=0){
                $display='block';
            }
            if(sizeof($results1)!=0){
                $display1='block';
            }
            return $this->render("default/frontRenderVous.html.twig",array('pagination'=>$results,'pagination1'=>$results1,'display'=>$display,'display1'=>$display1));
        }
        return $this->redirectToRoute("fos_user_security_login");
    }

    public function ajoutSessAction(Request $request) {
        $session=new Session();
        $dep=$request->request->get('dep');
        $locale=$request->request->get('locale');
        $somme=$request->request->get('somme');
        $heure=$request->request->get('heure');
        $titre=$request->request->get('titre');
        $date=$request->request->get('date');
        $session->set('dep',$dep);
        $session->set('locale',$locale);
        $session->set('heure',$heure);
        $session->set('titre',$titre);
        $session->set('date',$date);
        $session->set('somme',$somme);
        return $this->redirectToRoute('soin_crm_ajout_calendrier');
    }

    public function getTarifAction(Request $request,$id) {
        $em = $this->get('doctrine.orm.entity_manager');
        $dep = $em->getRepository('AppBundle:Deplacement')->find($id);
        if($dep){
            if($dep->getType()==1){
                $tarif=60;
            }elseif($dep->getType()==2){
                $tarif=40;
            }else{
                $tarif=20;
            }
            $tar[]=['tar'=>$tarif,'dep'=>$dep->getTarif()];
        }else{
            $tarif=0;
            $tar[]=['tar'=>$tarif,'dep'=>0];

        }
        return new JsonResponse($tar);
    }

    public function ajoutAction(Request $request) {
        if($this->getUser()!=null){
            $em = $this->get('doctrine.orm.entity_manager');
            $session=new Session();
            $idDep=$session->get('dep');
            $locale=$session->get('locale');
            $heure=$session->get('heure');
            $date=$session->get('date');
            $titre=$session->get('titre');
            if($idDep=="")
                $idDep=0;
            $dql = "SELECT m "
                . "FROM AppBundle:Deplacement m where m.id=".$idDep ;
            $query = $em->createQuery($dql);
            $results = $query->getResult();
            $somme=0;
            $somme+=$results?$results[0]->getTarif():0;
            $agenda = new Agenda();
            $produits = $em->getRepository('AppBundle:Produit')->findAll();
            foreach ($produits as $prod){
                $id=$session->get($prod->getId());
                if($id){
                    $produit = $em->getRepository('AppBundle:Produit')->find($id);
                    $agenda->addProduit($produit);
                }
            }
            $user=$this->getUser();
            $agenda->setTitre($titre);
            $agenda->setDescription($titre);
            $agenda->setDeplacementid($results?$results[0]:null);
            $agenda->setHeureD(new \DateTime($heure));
            $agenda->setHeure(new \DateTime($heure));
            $agenda->setDate(new \DateTime($date));
            $dateex=new \DateTime($date);
            $interval = new \DateInterval('P1M');
            $dateex->add($interval);
            $agenda->setDateex($dateex);
            if($results){
                $nbHeure=$agenda->getDeplacementid()->getHeure();
                $nbMinute=$agenda->getDeplacementid()->getMinute();
                $time=$agenda->getHeure();
                $timeRetour=$agenda->getHeureD();
                if($nbHeure!=null){
                    $time->sub(new \DateInterval('PT'.$nbHeure.'H'));
                    $timeRetour->add(new \DateInterval('PT'.$nbHeure.'H'));

                }
                if($nbMinute!=null){
                    $time->sub(new \DateInterval('PT'.$nbMinute.'M'));
                    $timeRetour->add(new \DateInterval('PT'.$nbMinute.'M'));
                }
                $agenda->setTempsSortie($time);
                $agenda->setHeureR($timeRetour);
            }
            $agenda->setType(1);
            $agenda->setClientid($user);
            $agenda->setLocale($locale);
            $agenda->setIsDeleted(1);
            $agenda->setHeureD(new \DateTime($heure));
            $agenda->setHeure(new \DateTime($heure));
            $em = $this->getDoctrine()->getManager();
            $rendezVous = $em->getRepository('AppBundle:Agenda')->findBy(array('isDeleted'=>1));
            $existe=0;
            foreach($rendezVous as $rend){
                if($rend->getDate()->format('YY-mm-dd')==$agenda->getDate()->format('YY-mm-dd')){
                    if($rend->getHeureR()!=null && $rend->getTempsSortie()!=null){
                        if($agenda->getTempsSortie()!=null){
                            if(($agenda->getTempsSortie()->format('H:I')<=$rend->getHeureR()->format('H:I'))&&($agenda->getTempsSortie()->format('H:I')>=$rend->getTempsSortie()->format('H:I'))){
                                $existe=1;
                            }
                        }else{
                            if(($agenda->getHeure()->format('H:I')<=$rend->getHeureR()->format('H:I'))&&($agenda->getHeure()->format('H:I')>=$rend->getTempsSortie()->format('H:I'))){
                                $existe=1;
                            }
                        }

                    }else{
                        if($agenda->getTempsSortie()!=null){
                            if(($agenda->getTempsSortie()->format('H:I')<=$rend->getHeure()->format('H:I'))){
                                $existe=1;
                            }
                        }else{
                            if(($agenda->getHeure()->format('H:I')==$rend->getHeure()->format('H:I'))){
                                $existe=1;
                            }
                        }
                    }
                }
            }
            if($existe==1){
                $flash = array(
                    'key' => 'warning',
                    'title' => 'Désolée',
                    'msg' => "Cette heure n'est pas disponible, Veuillez choisir une autre plage horaire ");
                $this->setFlash($flash);
                return new Response('echec');
            }
            else {
                $somme1=0;
                foreach ($agenda->getProduitid() as $prod){
                    $somme1+=$prod->getPrix();
                }
                $som=$session->get('somme');
                if($agenda->getDeplacementid()){
                    if($agenda->getDeplacementid()->getType()==1){
                        if($som<60){
                            $flash = array(
                                'key' => 'warning',
                                'title' => 'Désolée',
                                'msg' => "pour un déplacement de 25km à 33km il faut au minimun 60€ d'achats");
                            $this->setFlash($flash);
                            return new Response('echec');
                        }

                    }elseif ($agenda->getDeplacementid()->getType()==2){
                        if($som<40){
                            $flash = array(
                                'key' => 'warning',
                                'title' => 'Désolée',
                                'msg' => "pour un déplacement de 10km à 24km il faut au minimun 40€ d'achats");
                            $this->setFlash($flash);
                            return new Response('echec');
                        }
                    }else{
                        if($som<20){
                            $flash = array(
                                'key' => 'warning',
                                'title' => 'Désolée',
                                'msg' => "pour un déplacement de 0km à 09km il faut au minimun 20€ d'achats");
                            $this->setFlash($flash);
                            return new Response('echec');
                        }
                    }
                }

                $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $res = "";
                for ($i = 0; $i < 10; $i++) {
                    $res .= $chars[mt_rand(0, strlen($chars)-1)];
                }
                $agenda->setCoupon($res);
                $em->persist($agenda);
                $em->flush();
                $pagination=array();
                $produits = $em->getRepository('AppBundle:Produit')->findBy(array('isDeleted'=>1));
                $flash = array(
                    'key' => 'success',
                    'title' => 'succée',
                    'msg' => "Votre rendez-vous est bien enregistré , votre Code est ".$res);
                $this->setFlash($flash);
                foreach ($produits as $res){
                    $produit=$session->get($res->getId());
                    if($produit){
                        $pagination[]=$res;
                        $somme+=$res->getPrix();
                    }
                }
                foreach($produits as $rend){
                    $session->remove($rend->getId());
                }
                //to patronne
                $to = "arfaouiikram2015@gmail.com";
                $subject = "réservation confirmée";
                $txt = "réservation confirmée";
                $headers = "From: ".$this->getUser()->getEmail();
                mail($to,$subject,$txt,$headers);
                //to cliente
                $em = $this->get('doctrine.orm.entity_manager');
                $dql = "SELECT m "
                    . "FROM AppBundle:Agenda m  where m.isDeleted=1 ORDER BY m.id DESC ";
                $query = $em->createQuery($dql);
                $results = $query->getResult();
                $somme=0;
                if($results[0]->getDeplacementId()){
                    $somme+=$results[0]->getDeplacementId()->getTarif();
                }
                foreach($results[0]->getProduitId() as $p){
                    $somme+=$p->getPrix();
                }
                $html =$this->renderView("default/mailreservation.html.twig",array('pagination'=>$results[0]->getProduitId(),'somme'=>$somme,'agenda'=>$results[0],'user'=>$this->getUser()));
                $to = $this->getUser()->getEmail();
                $subject = "paiement enregistré";
                $txt = "to cliente";
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "From: arfaouiikram2015@gmail.com";
                mail($to,$subject,$html,$headers);
                return new Response('ok');

            }
        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    public function ajoutAdminAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $titre=$request->request->get('title');
        $debut=$request->request->get('start')+"";
        $year=$request->request->get('year')+"";
        $month=$request->request->get('month')+"";
        $day=$request->request->get('day')+"";
        $hour=$request->request->get('hour')+"";
        $second=$request->request->get('second')+"";
        $fin=$request->request->get('end');
        $agenda = new Agenda();
        $user=$this->getUser();
          $agenda->setDate(new \DateTime($year."-".$month."-".$day));
          $agenda->setHeure(new \DateTime($hour.":".$second));
          $agenda->setHeureD(new \DateTime($hour.":".$second));
        $agenda->setTitre($titre);
        $agenda->setDescription($titre);
        $agenda->setType(1);
        $agenda->setIsDeleted(1);
        $agenda->setClientid($user);
        $em = $this->getDoctrine()->getManager();
        $rendezVous = $em->getRepository('AppBundle:Agenda')->findBy(array('isDeleted'=>1));
        $existe=0;
        foreach($rendezVous as $rend){
            if($rend->getDate()->format('YY-mm-dd')==$agenda->getDate()->format('YY-mm-dd')){
                if(($rend->getHeureR()!=null) && ($rend->getTempsSortie()!=null)){
                    if(($agenda->getHeureD()->format('H:I')<=$rend->getHeureR()->format('H:I'))&&($agenda->getHeureD()->format('H:I')>=$rend->getTempsSortie()->format('H:I'))){
                        $existe=1;
                    }
                }
            }
        }
        if($existe==1){
            $flash = array(
                'key' => 'warning',
                'title' => 'Échec',
                'msg' => "Cette heure n'est disponible");
            $this->setFlash($flash);

        }else {
            $em->persist($agenda);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'succée',
                'msg' => "Votre rendez-vous est bien enregistré");
            $this->setFlash($flash);
        }
        return new Response(1);
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('agenda', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $agenda = $em->getRepository('AppBundle:Agenda')->find($id);
        $date1=new \DateTime($agenda->getDate()->format('y-m-d'));
        $date=new \DateTime($agenda->getDate()->format('y-m-d'));
        $date->sub(new \DateInterval('P2D'));
        $nox=new \DateTime();
        $format=$nox->format('y-m-d');
        $now=new \DateTime($format);
        if($now->format('y-m-d')<=$date->format('y-m-d') || $now->format('y-m-d')>=$date1->format('y-m-d')){
            $agenda->setIsDeleted(0);
            $em->persist($agenda);
            $em->flush();
        }else{
            $flash = array(
                'key' => 'warning',
                'title' => 'Échec',
                'msg' => "Vous ne pouvez  pas annuler qu’avant 48h");
            $this->setFlash($flash);
        }

        return $this->redirect($this->generateUrl("soin_crm_calendrier_list"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $form = $this->createForm(ActualiteForm::class, $actualite);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $actualite->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/actualite',
                    $fileName
                );
                $actualite->setLogo($fileName);
                $actualite->setFile(null);
            }
            $em->persist($actualite);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_actualite"));

        }
        return $this->render('default/ajoutActualite.html.twig', array('form' => $form->createView(),'logo'=>$actualite->getLogo(),'display'=>'none'));
    }

    public function couponAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $cadeau = $em->getRepository('AppBundle:Cheque')->find($id);
        $template = 'base1.html.twig';
        $filename = sprintf('Facture-%s.pdf', date('d-m-Y'));
        $html = $this->renderView($template,array('entity'=>$cadeau));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
//                    'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }

}
