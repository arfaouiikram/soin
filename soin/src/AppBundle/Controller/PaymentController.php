<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Form\ClientForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\RechForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RequestContext;


class PaymentController extends Controller
{

    public function payementAction (Request $request ,$totale,$type){
        $user=$this->getUser();
        if($user){
            $baseurl = $request->getScheme() . '://' . $request->getHttpHost() .$request->getBaseUrl();
            $user="arfaouiikram2016-acheteur_api1.gmail.com";
            $password="X5JZMLQQDKL5AM9G";
            $signature="AFcWxV21C7fd0v3bYYYRCpSSRl31AzgnTx-oZ1aJ9AyoLaI4Z56o24.A";
            /*  $user = $this->getUser();
              $em = $this->getDoctrine()->getManager();
              $dql = "SELECT l "
                  . "FROM AppBundle:LigneCommande l"
                  . " join l.Commandeid c"
                  . " join c.Userid u"
                  . " where u.id=".$user->getId()
                  . " and c.isDeleted=1" ;
              $query = $em->createQuery($dql);
              $lignes = $query->getResult();*/
            $param=array(
                'METHOD'=>'SetExpressCheckout',
                'VERSION'=>'95.0',
                'USER'=>$user,
                'SIGNATURE'=>$signature,
                'PWD'=>$password,
                'RETURNURL'=>$baseurl.'/details?totale='.$totale."&type=".$type,
                'CANCELURL'=>$baseurl.'/panier',
                'PAYMENTREQUEST_0_AMT'=>$totale,
                'PAYMENTREQUEST_0_CURRENCYCODE'=>'EUR',
                'PAYMENTREQUEST_0_ITEMAMT'=>$totale

            );
          /*  $em = $this->get('doctrine.orm.entity_manager');
            $dql = "SELECT m "
                . "FROM AppBundle:Agenda m  where m.isDeleted=1 ORDER BY m.id DESC ";
            $query = $em->createQuery($dql);
            $results = $query->getResult();
            if($results){
                foreach ($results[0]->getProduitid() as $k=>$ligne){
                    $param["L_PAYMENTREQUEST_0_NAME$k"]=$ligne->getDesignation();
                    $param["L_PAYMENTREQUEST_0_AMT$k"]=$ligne->getPrix();
                    $param["L_PAYMENTREQUEST_0_QTY$k"]=1;
                }
            }*/
            $param=http_build_query($param);
            $endponit='https://api-3T.sandbox.paypal.com/nvp';
            $curl=curl_init();
            curl_setopt_array($curl,array(
                CURLOPT_URL=>$endponit,
                CURLOPT_POST=>1,
                CURLOPT_POSTFIELDS=>$param,
                CURLOPT_RETURNTRANSFER=>1,
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_SSL_VERIFYHOST=>false,
                CURLOPT_VERBOSE=>1
            ));
            $response=curl_exec($curl);
            $tab=array();
            parse_str($response,$tab);
            $paypal="https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&useraction=commit&token=".$tab['TOKEN'];
            return $this->redirect($paypal);
        }
        return $this->redirectToRoute("fos_user_security_login");

    }

    public function detailsAction (Request $request ){
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() .$request->getBaseUrl();
        $user="arfaouiikram2016-acheteur_api1.gmail.com";
        $password="X5JZMLQQDKL5AM9G";
        $signature="AFcWxV21C7fd0v3bYYYRCpSSRl31AzgnTx-oZ1aJ9AyoLaI4Z56o24.A";
        $em = $this->getDoctrine()->getManager();
        $username = $this->getUser();
        if($user){
            $param=array(
                'METHOD'=>'GetExpressCheckoutDetails',
                'VERSION'=>'95.0',
                'TOKEN'=>$_GET['token'],
                'USER'=>$user,
                'SIGNATURE'=>$signature,
                'PWD'=>$password,
                'RETURNURL'=>$baseurl.'/payed',
                'CANCELURL'=>$baseurl.'/panier',
                'PAYMENTREQUEST_0_AMT'=>$_GET['totale'],
                'PAYMENTREQUEST_0_CURRENCYCODE'=>'EUR'

            );
            $param=http_build_query($param);
            $endponit='https://api-3T.sandbox.paypal.com/nvp';
            $curl=curl_init();
            curl_setopt_array($curl,array(
                CURLOPT_URL=>$endponit,
                CURLOPT_POST=>1,
                CURLOPT_POSTFIELDS=>$param,
                CURLOPT_RETURNTRANSFER=>1,
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_SSL_VERIFYHOST=>false,
                CURLOPT_VERBOSE=>1
            ));
            $response=curl_exec($curl);
            $tab=array();
            parse_str($response,$tab);
            if($tab['CHECKOUTSTATUS']!='PaymentActionCompleted'){
                //payment
                $param=array(
                    'METHOD'=>'DoExpressCheckoutPayment',
                    'VERSION'=>'95.0',
                    'TOKEN'=>$_GET['token'],
                    'PAYERID'=>$_GET['PayerID'],
                    'PAYMENTACTION'=>'Sale',
                    'USER'=>$user,
                    'SIGNATURE'=>$signature,
                    'PWD'=>$password,
                    'RETURNURL'=>$baseurl.'/payed',
                    'CANCELURL'=>$baseurl.'/panier',
                    'PAYMENTREQUEST_0_AMT'=>$_GET['totale'],
                    'PAYMENTREQUEST_0_CURRENCYCODE'=>'EUR'
                );
                $param=http_build_query($param);
                $curl=curl_init();
                curl_setopt_array($curl,array(
                    CURLOPT_URL=>$endponit,
                    CURLOPT_POST=>1,
                    CURLOPT_POSTFIELDS=>$param,
                    CURLOPT_RETURNTRANSFER=>1,
                    CURLOPT_SSL_VERIFYPEER=>false,
                    CURLOPT_SSL_VERIFYHOST=>false,
                    CURLOPT_VERBOSE=>1
                ));
                $response=curl_exec($curl);
                $tab=array();
                parse_str($response,$tab);
                $type=$request->query->get('type');
                if($type==1){
                    $to = "arfaouiikram2015@gmail.com";
                    $subject = "paiement confirmer";
                    $txt = "to patronne!";
                    $headers = "From: ".$this->getUser()->getEmail();
                    mail($to,$subject,$txt,$headers);
                    //to cliente
                    $em = $this->get('doctrine.orm.entity_manager');
                    $dql = "SELECT m "
                        . "FROM AppBundle:Agenda m  where m.isDeleted=1 ORDER BY m.id DESC ";
                    $query = $em->createQuery($dql);
                    $results = $query->getResult();
                    $somme=0;
                    if($results[0]->getDeplacementId()){
                        $somme+=$results[0]->getDeplacementId()->getTarif();
                    }
                    foreach($results[0]->getProduitId() as $p){
                        $somme+=$p->getPrix();
                    }
                    $html =$this->renderView("default/mailPaiement.html.twig",array('pagination'=>$results[0]->getProduitId(),'somme'=>$somme,'agenda'=>$results[0],'user'=>$this->getUser()));
                    $to = $this->getUser()->getEmail();
                    $subject = "paiement enregistré";

                    $em = $this->get('doctrine.orm.entity_manager');
                    $dql = "SELECT m "
                        . "FROM AppBundle:Agenda m  where m.isDeleted=1 ORDER BY m.id DESC ";
                    $query = $em->createQuery($dql);
                    $results = $query->getResult();
                    $results[0]->setPayed(1);
                    $em->persist($results[0]);
                    $em->flush();
                }else{
                    $em = $this->get('doctrine.orm.entity_manager');
                    $dql = "SELECT m "
                        . "FROM AppBundle:Cheque m  where m.isDeleted=1 ORDER BY m.id DESC ";
                    $cheque = $em->createQuery($dql)->getResult();
                    $session=new Session();
                    $nom=$session->get('nom');
                    $prenom=$session->get('prenom');
                    $mail=$session->get('mail');
                    $date=$session->get('date');
                    $coupon=$session->get('coupon');
                    $html= $this->renderView("default/mailchequeDes.html.twig",array('nom'=>$nom,'prenom'=>$prenom,'user'=>$this->getUser(),'date'=>$date,'coupon'=>$coupon,'mail'=>$mail,'cheque'=>$cheque[0]));
                    $to = $mail;
                    $subject = "Chèque Cadeau ";
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= "From: arfaouiikram2015@gmail.com";
                    mail($to,$subject,$html,$headers);
                    $subject = "Chèque Cadeau : Accusé";
                    $html= $this->renderView("default/mailchequeAch.html.twig",array('nom'=>$nom,'prenom'=>$prenom,'user'=>$this->getUser(),'date'=>$date,'coupon'=>$coupon,'mail'=>$mail));
                    $to = $this->getUser()->getEmail();
                    mail($to,$subject,$html,$headers);
                    $em = $this->get('doctrine.orm.entity_manager');
                    $dql = "SELECT m "
                        . "FROM AppBundle:Cheque m  where m.isDeleted=1 ORDER BY m.id DESC ";
                    $query = $em->createQuery($dql);
                    $results = $query->getResult();
                    $results[0]->setPayed(1);
                    $somme=$results[0]->getCheque()->getAbonnement();
                    $em->persist($results[0]);
                    $em->flush();
                    $html =$this->renderView("default/mailcheque.html.twig",array('somme'=>$somme,'cheque'=>$results[0],'user'=>$this->getUser()));
                    $to = $this->getUser()->getEmail();
                    $subject = "Chèque Cadeau : paiement enregistré";

                }
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "From: arfaouiikram2015@gmail.com";
                mail($to,$subject,$html,$headers);


            }
            $flash = array(
                'key' => 'warning',
                'title' => 'Succés',
                'msg' => "votre paiement est bien effectué ");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl('soin_crm_calendrier_list'));
        }
        $flash = array(
            'key' => 'warning',
            'title' => 'Echeck',
            'msg' => "Echeck lors de paiement ");
        $this->setFlash($flash);
        return $this->redirect($this->generateUrl('soin_crm_calendrier_list'));
    }
    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('paiement', $value);
    }



}

