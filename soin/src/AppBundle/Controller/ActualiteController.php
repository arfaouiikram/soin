<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Actualite;
use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ActualiteController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Actualite m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/actualite.html.twig",array('pagination' => $results));
    }

    public  function  cadeauAction(Request $request){
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Cadeau m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/frontCadeau.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $actualite = new Actualite();
        $form = $this->createForm(ActualiteForm::class, $actualite);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $actualite->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/actualite',
                    $fileName
                );
                $actualite->setLogo($fileName);
                $actualite->setFile(null);
            }
            $actualite->setIsDeleted(1);
            $em->persist($actualite);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $actualite = new Actualite();
                $form = $this->createForm(ActualiteForm::class, $actualite);
                return $this->render('default/ajoutActualite.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_actualite'));
        }
        return $this->render('default/ajoutActualite.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $actualite->setIsDeleted(0);
        $em->persist($actualite);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_actuliate"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $form = $this->createForm(ActualiteForm::class, $actualite);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $actualite->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/actualite',
                    $fileName
                );
                $actualite->setLogo($fileName);
                $actualite->setFile(null);
            }
            $em->persist($actualite);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_actualite"));

        }
        return $this->render('default/ajoutActualite.html.twig', array('form' => $form->createView(),'logo'=>$actualite->getLogo(),'display'=>'none'));
    }

}
