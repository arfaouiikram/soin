<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Cadeau;
use AppBundle\Entity\Cheque;
use AppBundle\Entity\Client;
use AppBundle\Entity\Famille;
use AppBundle\Form\CadeauForm;
use AppBundle\Form\ClientForm;
use AppBundle\Form\FamilleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;


class CadeauController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Cadeau m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/cadeau.html.twig",array('pagination' => $results));
    }


    public function chequeAction(Request $request){
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT m "
            . "FROM AppBundle:Cadeau m where m.isDeleted=1" ;
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        return $this->render("default/frontCadeau.html.twig",array('pagination' => $results));
    }

    public function ajoutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $cadeau = new Cadeau();
        $form = $this->createForm(CadeauForm::class, $cadeau);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $cadeau->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/cadeau',
                    $fileName
                );
                $cadeau->setLogo($fileName);
                $cadeau->setFile(null);
            }
            $cadeau->setIsDeleted(1);
            $em->persist($cadeau);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Marque ajoutée avec succés");
            $this->setFlash($flash);
            $nextAction = $request->request->has("ajout");
            if($nextAction==true)
            {
                $cadeau = new Cadeau();
                $form = $this->createForm(CadeauForm::class, $cadeau);
                return $this->render('default/ajoutCadeau.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
            }
            return $this->redirect($this->generateUrl('soin_crm_cadeau'));
        }
        return $this->render('default/ajoutCadeau.html.twig', array('form' => $form->createView(),'logo'=>null,'display'=>'block'));
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('alert', $value);
    }

    public function supprimerAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $cadeau = $em->getRepository('AppBundle:Cadeau')->find($id);
        $cadeau->setIsDeleted(0);
        $em->persist($cadeau);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_cadeau"));
    }

    public function suppAction(Request $request , $id){
        $em = $this->getDoctrine()->getManager();
        $cheque = $em->getRepository('AppBundle:Cheque')->find($id);
        $cheque->setIsDeleted(0);
        $em->persist($cheque);
        $em->flush();
        return $this->redirect($this->generateUrl("soin_crm_calendrier_list"));
    }

    public function modifierAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $cadeau = $em->getRepository('AppBundle:Cadeau')->find($id);
        $form = $this->createForm(CadeauForm::class, $cadeau);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $file = $cadeau->getFile();
            if($file!=null)
            {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                // Move the file to the directory where brochures are stored
                $file->move($this->container->getParameter('kernel.root_dir').'/../web/image/cadeau',
                    $fileName
                );
                $cadeau->setLogo($fileName);
                $cadeau->setFile(null);
            }
            $em->persist($cadeau);
            $em->flush();
            $flash = array(
                'key' => 'success',
                'title' => 'Succès',
                'msg' => "Client modofiée avec succés");
            $this->setFlash($flash);
            return $this->redirect($this->generateUrl("soin_crm_cadeau"));
        }
        return $this->render('default/ajoutCadeau.html.twig', array('form' => $form->createView(),'logo'=>$cadeau->getLogo(),'display'=>'none'));
    }

    public function frontAction(Request $request ,$id){
        if($this->getUser()){
            $id=$request->request->get('idcheque');
            $nom=$request->request->get('nom');
            $prenom=$request->request->get('prenom');
            $mail=$request->request->get('mail');
            $session=new Session();
            $session->set('nom',$nom);
            $session->set('prenom',$prenom);
            $session->set('mail',$mail);
            $em = $this->getDoctrine()->getManager();
            $cadeau = $em->getRepository('AppBundle:Cadeau')->find($id);
            $cheque=new Cheque();
            $date=new \DateTime();
            $interval = new \DateInterval('P3M');
            $date->add($interval);
            $session->set('date',$date);
            $cheque->setClientid($this->getUser());
            $cheque->setDate($date);
            $cheque->setCheque($cadeau);
            $cheque->setIsDeleted(1);
            $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $res = "";
            for ($i = 0; $i < 10; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            $cheque->setCoupon($res);
            $session->set('coupon',$res);
            $em->persist($cheque);
            $em->flush();
            return $this->redirectToRoute('simulation_crm_paypal',array('totale'=>$cadeau->getAbonnement()+"",'type'=>2));
        }
        return $this->redirectToRoute("fos_user_security_login");
    }

}
