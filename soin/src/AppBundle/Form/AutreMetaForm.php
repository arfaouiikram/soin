<?php

namespace AppBundle\Form;


use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\ProduitRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AutreMetaForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author')
            ->add('subject')
            ->add('revisitAfter')
            ->add('canonical')
            ->add('rating')
            ->add('organization')
            ->add('classification')
            ->add('language')
            ->add('location')
            ->add('distribution')
            ->add('audience')
            ->add('publisher')
            ->add('ogType')
            ->add('ogSiteName')
            ->add('ogImage')
            ->add('ogImageType')
            ->add('copyright')
            ->add('twitterCard')
            ->add('twitterCreator')
            ->add('twitterTitle')
            ->add('twitterDescription')
            ->add('twitterImage')


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AutreMetas'
        ));
    }

    public function getName()
    {
        return 'autremeta_form';
    }

}
