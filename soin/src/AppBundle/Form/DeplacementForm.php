<?php

namespace AppBundle\Form;


use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\ProduitRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DeplacementForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('designation')
            ->add('kilometre')
            ->add('tarif')
            ->add('heure')
            ->add('minute')
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    "25km à 33km" => '1',
                    "10km à 24km" => '2',
                    "0km à 9km" => '3'
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Deplacement'
        ));
    }

    public function getName()
    {
        return 'deplacement_form';
    }

}
