<?php

namespace AppBundle\Form;


use AppBundle\Entity\FamilleRepository;
use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\ProduitRepository;
use AppBundle\Entity\SousFamilleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('SousFamilleid', EntityType::class, array(
                'class' => 'AppBundle:SousFamille',
                'query_builder' => function (SousFamilleRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',))
            ->add('file',FileType::class, array(
                'label' => 'Logo (png , jpg , JPEG ...)',
                'required'=>false
            ))
            ->add('prix')
            ->add('dure')
            ->add('dureMin')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Produit'
        ));
    }

    public function getName()
    {
        return 'produit_form';
    }

}
