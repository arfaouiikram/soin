<?php

namespace AppBundle\Form;


use AppBundle\Entity\FamilleRepository;
use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\ProduitRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SousFamilleForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('Familleid', EntityType::class, array(
                'class' => 'AppBundle:Famille',
                'query_builder' => function (FamilleRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',))
            ->add('file',FileType::class, array(
                'label' => 'Logo (png , jpg , JPEG ...)',
                'required'=>false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SousFamille'
        ));
    }

    public function getName()
    {
        return 'sous_famille_form';
    }

}
