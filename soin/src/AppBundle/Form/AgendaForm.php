<?php

namespace AppBundle\Form;


use AppBundle\Entity\ClientRepository;
use AppBundle\Entity\DeplacementRepository;
use AppBundle\Entity\FamilleRepository;
use AppBundle\Entity\ModeleRepository;
use AppBundle\Entity\ProduitRepository;
use AppBundle\Entity\UserRepository;
use Doctrine\DBAL\Types\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class AgendaForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description')
            ->add('Deplacementid', EntityType::class, array(
                'class' => 'AppBundle:Deplacement',
                'query_builder' => function (DeplacementRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',))
            ->add('Produitid', EntityType::class, array(
                'class' => 'AppBundle:Produit',
                'query_builder' => function (ProduitRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.designation', 'ASC');
                },
                'choice_label' => 'designation',
                'multiple'  => true,
            ))
            ->add('Clientid', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('date', \Symfony\Component\Form\Extension\Core\Type\DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Travail' => '1',
                    'Autre' => '2',
                ),
            ))
            ->add('lieu')
            ->add('heure', TimeType::class, array(
                'widget' => 'single_text',
            ))
            ->add('heureD', TimeType::class, array(
                'widget' => 'single_text',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Agenda'
        ));
    }

    public function getName()
    {
        return 'agenda_form';
    }

}
